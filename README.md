# prettymaps

A Docker image for [marceloprates/prettymaps](https://github.com/marceloprates/prettymaps).

> A minimal Python library to draw customized maps from [OpenStreetMap](https://www.openstreetmap.org/#map=12/11.0733/106.3078)
> created using the [osmnx](https://github.com/gboeing/osmnx),
> [matplotlib](https://matplotlib.org/),
> [shapely](https://shapely.readthedocs.io/en/stable/index.html)
> and [vsketch](https://github.com/abey79/vsketch) libraries.

This image is mostly a packaging convenience:
the complete  `prettymaps` stack in a single 1.2Gb bundle,
ready to shoot.

## Usage

The `WORKDIR` is `/src/`.

You may want to bind-mount your python code there:

```shell
docker run --rm -v $(pwd):/src letompouce/prettymaps
```

Then, in the container, run your code:

```shell
python somecode.py
```

## Cache

You may want to use a globally shared cache folder,
so it is used wherever you run `prettymaps` from:

```shell
docker run --rm \
    -v $(pwd):/src \
    -v ${$XDG_CACHE_HOME}/prettymaps:/src/cache \
    letompouce/prettymaps \
        python somecode.py
```

## curl|sh in a container world

Let's put all of this in this convenient `.bashrc` alias:

```shell
type -t docker &>/dev/null \
    && alias prettymaps='docker run --rm \
        -v $(pwd):/src \
        -v $XDG_CACHE_HOME/prettymaps:/src/cache \
        letompouce/prettymaps \
            python'
```

## Links

* Source: <https://gitlab.com/l3tompouce/docker/prettymaps>
* Image: <https://hub.docker.com/r/letompouce/prettymaps>
